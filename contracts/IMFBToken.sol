// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

interface IMFBToken{
    function burnOnBuy(address caller, uint amount) external returns (bool);
    function mint(uint amount) external returns (bool);
    function approve2Contract(address adr,uint amount) external returns (uint);
    function cleanApprove(address adr) external returns (uint);
}
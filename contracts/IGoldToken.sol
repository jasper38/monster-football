// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

interface IGoldToken{
    function mintGold(address owner,uint amount) external returns (uint256);
    function burnGold(address owner, uint amount) external returns (uint256);
}
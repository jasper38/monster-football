// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;
pragma abicoder v2;
import "./contracts/access/Ownable.sol";
import "./contracts/utils/Counters.sol";
import "./contracts/token/ERC20/IERC20.sol";
import "./contracts/token/ERC721/IERC721.sol";
import "./Access.sol";
import "./IMFEToken.sol";
import "./IMFBToken.sol";
import "./MFBPool.sol";
import "./AddressCenter.sol";
import "./IPancake.sol";
import "./contracts/utils/math/SafeMath.sol";
import "./contracts/utils/Strings.sol";
import "./TokenChannel.sol";
import "./RebatePool.sol";

contract BlindBox is Ownable{   
    //address
    AddressCenter adrCenter = AddressCenter(0x6dd9D2dbfFe58993102cfF7a034649357f9E0f5c);
    Access acs = Access(0x88C96DCcD894E4bcb152eE26ab37888936f47074);
    address public _mfb_adr;
    address public _nft_contract;
    address public _pool_adr;
    address public _op_wallet ;
    //address public _invite_contract;
    address public _pancake_adr;
    address public _token_channel;
    address public _rebate_adr;
    address public zero = 0x0000000000000000000000000000000000000000;
    //address end
    //variables
    using SafeMath for uint256;
    using Counters for Counters.Counter;
	Counters.Counter private _adIds;     
    struct advertise{
        uint id;
        uint startTime;
        uint endTime;
        string name;
        uint price;
        bool state;
        uint amount;
    }
    uint[] all_record;
    uint[] on_sale;
    //fee setting
    uint public burnRate = 60;
    uint public op_fee = 10;
    uint public pool_fee = 18;
    uint public invite_fee = 12;
    mapping(address => uint) public box_per_day;
    address [] users;
    mapping(uint => advertise) public advertises;
    //modfier
    modifier onlyOperator{
        require(acs.operators(msg.sender),"Require operator permission");
        _;
    }
    //event
    event buyBox(address buyer, uint price, uint amount);
    event closeBox(uint adId);
    event toAddress(address buyer, uint amount, string t_type);
    event addAd(uint id, string name, uint startTime, uint endTime, uint price, bool state, uint amount);
    constructor(){}
    //initialize
    function initialize_adr() public onlyOperator {
        _mfb_adr = adrCenter.mfb();
        _nft_contract = adrCenter.nft();
        _pool_adr = adrCenter.mfb_pool();
        _op_wallet = adrCenter.op_wallet();
        _pancake_adr = adrCenter.pancake();
        _token_channel = adrCenter.channel();
        _rebate_adr = adrCenter.rebate();
    }
    //fee setting
    function setBurnRate (uint rate) public onlyOperator returns (bool) {
        burnRate = rate;
        return true;
    }
    function setOpFee (uint rate) public onlyOperator returns (bool) {
        op_fee = rate;
        return true;
    }
    function setPoolRate (uint rate) public onlyOperator returns (bool) {
        pool_fee = rate;
        return true;
    }
    function setInviteRate (uint rate) public onlyOperator returns (bool) {
        invite_fee = rate;
        return true;
    }
    //fee setting end
    //advertises utils
    function pushIn(uint id) public onlyOperator {
        all_record.push(id);
    }
    //查詢
    function AllList() public view returns (uint[] memory){
        return all_record;
    }
    //重設每日購買盒數
    function resetAllBox () public {
        for (uint i=0; i< users.length ; i++){
            box_per_day[users[i]] = 0;
        }
    }
    //紀錄盲盒狀態
    function addAdvertise(string memory name,uint startTime, uint endTime, uint price, bool state, uint amount)
    public
    onlyOperator
    returns (uint)
    {
        _adIds.increment();
        uint256 id = _adIds.current();
        advertises[id].id = id;
        advertises[id].name = name;
        advertises[id].startTime = startTime;
        advertises[id].endTime = endTime;
        advertises[id].price = price;
        advertises[id].state = state;
        advertises[id].amount = amount;
        pushIn(id);
        emit addAd(id,name,startTime,endTime,price,state,amount);
        return id;
    }
    function setClose(uint id) public onlyOperator{
        advertises[id].state = false;
        emit closeBox(id);
    }
    function time() public view returns (uint){
        return block.timestamp;
    }
    function buyBoxByMFB(uint id, uint amount, uint price, uint nonce, bytes memory signature) public{
        require(advertises[id].amount >= amount,"remain box is not enough");
        require(advertises[id].startTime < time(),"Blind box selling is not yet started.");
        require(advertises[id].endTime > time(),"Blind box selling is end");
        require(advertises[id].state == true,"Blind box selling is closed");
        require(amount + box_per_day[msg.sender] <= 10,"Exceed the box purchase limit");
        require(TokenChannel(_token_channel).checkBox(price,nonce,signature),"Signature error");
        require(price.mul(100).div(calPrice()) > 5,"Price Error!");
        //Token transfer
        uint total = amount * advertises[id].price * calPrice() / 100; // calculate the total amount divide 100
        uint percent = pool_fee + invite_fee + op_fee; // skip divide 100 
        //address invitee = Invite(_invite_contract).invites_reverse(msg.sender);
        IMFBToken(_mfb_adr).approve2Contract(msg.sender,total * percent);
        //獎勵池
        IERC20(_mfb_adr).transferFrom(msg.sender,_pool_adr, total * pool_fee);  
        emit toAddress(msg.sender,total * pool_fee ,"pool");
        //反傭池
        IERC20(_mfb_adr).transferFrom(msg.sender,_rebate_adr, total * invite_fee);  
        emit toAddress(msg.sender,total * invite_fee ,"rebate");
        //錢包
        IERC20(_mfb_adr).transferFrom(msg.sender,_op_wallet,total * op_fee);
        IMFBToken(_mfb_adr).burnOnBuy(msg.sender,total * burnRate);
        box_per_day[msg.sender] += amount;
        users.push(msg.sender);
        emit buyBox(msg.sender, calPrice(), amount);
        for(uint i=0; i< amount; i++){
            IMFEToken(_nft_contract).openBox(msg.sender);
        }
        advertises[id].amount -= amount;
    }
    function pancakePrice(bool isLocal) internal view returns (uint, uint, uint){
        if(isLocal){
            return (1,100,100);
        }
        else{
            return IPancake(_pancake_adr).getReserves();
        }
    }
    function calPrice() public view returns (uint){
        uint r1;
        uint r2;
        uint b; 
        (r1, r2, b) = pancakePrice(false); //local
        r2 = r2.mul(10 ** 18);
        r2 = r2.div(r1);
        return r2;
    }
    function getList() public view returns (string memory){  
        string memory list_str = "";    
        for (uint i=0; i< all_record.length ; i++){
            if(advertises[all_record[i]].state == true && advertises[all_record[i]].endTime >= block.timestamp){
                string memory id_string = Strings.toString(all_record[i]);
                if(i > 0){
                    list_str =  string(abi.encodePacked(list_str,","));
                }               
                list_str =  string(abi.encodePacked(list_str,id_string));
            }         
        }
        return list_str;
    }
}
// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./contracts/token/ERC721/ERC721.sol";
import "./contracts/utils/Counters.sol";
import "./contracts/access/Ownable.sol";
import "./contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "./Access.sol";
import "./contracts/utils/Strings.sol";
import "./AddressCenter.sol";

contract MFEToken is ERC721URIStorage, Ownable{
    //variables
    using Counters for Counters.Counter;
	Counters.Counter private _tokenIds;
    struct monster{
        uint reproduction;
        uint ap;
    }
    mapping(uint => monster) public monsters;
    mapping(uint => uint) public states; 
    /**
    0: normal
    1: working
    2: dead
    3: selling
    4: inGame 
    **/
    //address
    AddressCenter adrCenter = AddressCenter(0x6dd9D2dbfFe58993102cfF7a034649357f9E0f5c);
    Access acs = Access(0x88C96DCcD894E4bcb152eE26ab37888936f47074);
    address public _box_adr;
    address _market_adr;
    address _gamefi_adr;
    bool public is_init = false;
    string public uriPrefix = "test";
    uint public totalSupply = 0;
    //modfier
    modifier onlyOperator{
        require(acs.operators(msg.sender),"Require operator permission");
        _;
    }
    //event
    event mintDone(address owner,uint id);
    event combineDone(address owner,uint id);
    event stateChg(uint id, uint state);
    constructor() ERC721("Monster Football Egg","MFE"){}
    //initialize
    function initialize_adr() public onlyOperator {
        _box_adr = adrCenter.blindbox();
        _market_adr = adrCenter.nft_market();
        _gamefi_adr = adrCenter.gamefi();
    }
    function mintNFT(address recipient, string memory tokenURI)
    private
    returns (uint256)
    {
        _tokenIds.increment();
        uint256 newItemId = _tokenIds.current();
        monsters[newItemId].reproduction = 7;
        states[newItemId] = 0;
        _mint(recipient, newItemId);
        _setTokenURI(newItemId, tokenURI);
        totalSupply += 1;
        return newItemId;
    }
    function burnNFT(uint256 tokenId)
        public
        onlyOperator
    {
        _burn(tokenId);
        totalSupply -= 1;
    }
    //合併NFT
    function combination(uint tokenId_1,uint tokenId_2,address caller,string memory tokenURI)
    external
    returns (uint256)
    {
        require(ownerOf(tokenId_1) == caller,"you are not token owner!");
        require(ownerOf(tokenId_2) == caller,"you are not token owner!");
        require(monsters[tokenId_1].reproduction > 0,"Remain times insufficient!");
        require(monsters[tokenId_2].reproduction > 0,"Remain times insufficient!");
        monsters[tokenId_1].reproduction -= 1;
        monsters[tokenId_2].reproduction -= 1;
        mintNFT(caller,tokenURI);
        emit combineDone(msg.sender,_tokenIds.current());
        return _tokenIds.current();
    }
    //盲盒轉NFT
    function openBox(address caller) external returns (uint) {
        require(msg.sender == _box_adr,"This function can only be triggered by blind box contract!");
        uint256 newItemId = _tokenIds.current() + 1;
        string memory tokenURI = generateURIbyId(newItemId);
        mintNFT(caller,tokenURI);
        return _tokenIds.current();
    }
    //NFT更新屬性
    function updateProperties(uint id, uint ap, uint state) public{
        bool flag = false;
        if(acs.operators(msg.sender)){
            flag = true;
        }
        else if(msg.sender == _gamefi_adr){
            flag = true;
        }
        if(flag){
            if(monsters[id].ap != ap && ap != 999){
                monsters[id].ap = ap;
            }
            if(states[id] != state && state != 999){
                states[id] = state;
            }
        }
    }
    //transfer
    function transferFrom(
        address from,
        address to,
        uint256 tokenId
    ) public virtual override {
        //solhint-disable-next-line max-line-length
        require(_isApprovedOrOwner(_msgSender(), tokenId), "ERC721: transfer caller is not owner nor approved");
        require(states[tokenId] == 0,"NFT is not available");
        _transfer(from, to, tokenId);
    }
    function stateChange(uint id, uint state) external{
        bool flag = false;
        if(msg.sender == _market_adr){
            flag = true;
        }
        if(flag){
            states[id] = state;
            emit stateChg(id, state);
        }
    }
    //get state of nft
    function getState(uint tokenId) public view returns(uint) {
        return states[tokenId];
    }
    //generate tokenURI
    function generateURIbyId(uint id) private view returns (string memory){
        string memory id_string = Strings.toString(id);
        string memory pre = string(abi.encodePacked("https://",uriPrefix));
        pre = string(abi.encodePacked(pre,"/"));
        return string(abi.encodePacked(pre,id_string)); 
    }
    function setURIPrefix(string memory prefix) public onlyOperator returns (string memory){
        uriPrefix = prefix;
        return uriPrefix;
    }
}
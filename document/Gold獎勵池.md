# GoldPool.sol
## 領取獎勵
- function claimGold
- 領取獎勵
- cost: 0.00055297BNB

## 開始打工
- function startPartTime
- 開始打工
- body:
```
{
    "1"
}
```

## 結束打工
- function endPartTime
- 結束打工
- body:
```
{
    "1"
}
```

## 查詢獎勵
- function goldRewards
- body:
```
{
    "0x1ea2ddba5801Fc01EF4Ab4436874f27b08e28c0c"
}
```
- response:
```
{
    "reward": 100000000000000000000
}
```
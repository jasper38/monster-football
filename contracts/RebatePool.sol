// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;
import "./contracts/access/Ownable.sol";
import "./contracts/token/ERC20/IERC20.sol";
import "./Access.sol";
import "./IMFBToken.sol";
import "./AddressCenter.sol";
import "./TokenChannel.sol";

contract RebatePool is Ownable{
    //address
    AddressCenter adrCenter = AddressCenter(0x6dd9D2dbfFe58993102cfF7a034649357f9E0f5c);
    Access acs = Access(0x88C96DCcD894E4bcb152eE26ab37888936f47074);
    //variables
    address _token_adr;
    address _token_channel;
    //modfier
    modifier onlyOperator{
        require(acs.operators(msg.sender),"Require operator permission");
        _;
    }
    //event
    event claimRewardEvent(address user, uint amount, string reward_type);
    //initialize
    function initialize_adr() public onlyOperator {
        _token_adr = adrCenter.mfb();
        _token_channel = adrCenter.channel();
    }
    //領取獎勵
    //You can only take your portion.
    function claimReward(uint amount, uint nonce, bytes memory signature)
    public
    returns(bool)
    {
        require(IERC20(_token_adr).balanceOf(address(this)) >= amount,"Not enough token supply!");
        require(TokenChannel(_token_channel).check(amount,nonce,signature),"Amount error");
        emit claimRewardEvent(msg.sender,amount,"rebate");
        //IMFBToken(token_adr).cleanApprove(msg.sender);
        return true;
    }
    //Contract balance 
    function c_bal() public view returns (uint){
        return IERC20(_token_adr).balanceOf(address(this));
    }
}
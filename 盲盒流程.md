## 盲盒流程
### BlindBox.sol
1. 調用  addAdvertise(name, startTime, endTime, price, state, amount) 上架盲盒id會自動增加
>	ex:
>>	    name: BlindBox1
>>	    startTime: 1640154963
>>	    endTime: 1640164963
>>	    price: 1000000000000000000(18位數) => 1 MFB
>>	    state: true(盲盒狀態)
>>	    amount: 500 
- 監聽event addAd獲取(id,name,startTime,endTime,price,state,amount)
- 可調用getList()獲取廣告id列表，因合約無法自動去同步狀態提供兩種方法查詢
    1. 用getList()會將超過時間或是設定取消的排除回傳
    2. 用AllList()查全部列表，再從advertises(id)獲取狀態去判斷是否還在販賣中
2. 調用 BlindBox setClose(id) 手動下架盲盒
3. 調用 resetAllBox() 重置每日可購買上限
4. 調用 box_per_day(address) 可以查詢每日購買盲盒
5. 使用 buyBoxByMFB(id,amount,price,nonce) 購買盲盒 
   - 此處檢查address MFB餘額是否足夠，每日可購買盲盒是否超過上限
   - 設置獎勵移至MFBPool合約及RebatePool合約
   - 此處檢查價格與Pancake上的價格差異
   - 請監聽toAddress(address,uint,"pool")及toAddress(address,uint,"rebate")

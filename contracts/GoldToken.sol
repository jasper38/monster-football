// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;
import "./AddressCenter.sol";
import "./contracts/token/ERC20/ERC20.sol";
import "./contracts/access/Ownable.sol";
import "./Access.sol";
import "./TokenChannel.sol";

contract GoldToken is ERC20, Ownable{
    AddressCenter adrCenter = AddressCenter(0x6dd9D2dbfFe58993102cfF7a034649357f9E0f5c);
    Access acs = Access(0x88C96DCcD894E4bcb152eE26ab37888936f47074);
    address _token_channel;
    event GoldClaim(address user, uint amount);
    //modfier
    modifier onlyOperator{
        require(acs.operators(msg.sender),"Require operator permission");
        _;
    }
    constructor() ERC20("gold", "GD"){}
    //initialize
    function initialize_adr() public onlyOperator {
        _token_channel = adrCenter.channel();
    }
    function mintGold(uint amount, uint nonce, bytes memory signature)
        public
        returns (uint256)
    {
        require(TokenChannel(_token_channel).check(amount,nonce,signature),"Amount error");
        _mint(msg.sender, amount);
        emit GoldClaim(msg.sender, amount);
        return amount;
    }
    function burnGold(address owner, uint amount)
        public
        returns (uint256)
    {
        _burn(owner, amount * 10 ** 18);
        return amount;
    }
}
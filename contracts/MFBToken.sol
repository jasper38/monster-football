// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./contracts/token/ERC20/ERC20.sol";
import "./contracts/access/Ownable.sol";
import "./Access.sol";
import "./AddressCenter.sol";
import "./MFBPool.sol";

contract MFBToken is ERC20, Ownable{  
    //address
    AddressCenter adrCenter = AddressCenter(0x6dd9D2dbfFe58993102cfF7a034649357f9E0f5c);
    Access acs = Access(0x88C96DCcD894E4bcb152eE26ab37888936f47074);
    address public _box_adr;
    address _market_adr;
    address _pool_adr;
    address _op_wallet;
    address _pancake;
    address _router;
    event depositToken(address,uint);
    //modfier
    modifier onlyOperator{
        require(acs.operators(msg.sender),"Require operator permission");
        _;
    }
    constructor() ERC20("monster football token", "MFB"){
        _mint(address(this), 100000000 * 10 ** 18);
        _mint(msg.sender, 100000000 * 10 ** 18);
    }
    //initialize
    function initialize_adr() public onlyOperator {
        _box_adr = adrCenter.blindbox();
        _market_adr = adrCenter.nft_market();
        _pool_adr = adrCenter.mfb_pool();
        _op_wallet = adrCenter.op_wallet();
        _pancake = adrCenter.pancake();
        _router = adrCenter.pancake_router();
    }
    function approve2Contract(address adr,uint amount) external returns (uint){
        if(msg.sender == _box_adr){
            _approve(adr,_box_adr,amount);
        }
        if(msg.sender == _market_adr){
            _approve(adr,_market_adr,amount);
        }
        if(msg.sender == _pool_adr){
            _approve(_pool_adr,adr,amount);
        }
        return allowance(msg.sender,adr);
    }
    function cleanApprove(address adr) external returns (uint){
        bool flag = false;
        if(msg.sender == _pool_adr){
            flag = true;
            _approve(adr,_pool_adr,0);
        }
        return allowance(msg.sender,_pool_adr);
    }
    function burnOnBuy(address caller, uint amount) external returns (bool){
        bool flag = false;
        if(msg.sender == _box_adr){
            flag = true;
        }
        if(msg.sender == _market_adr){
            flag = true;
        }
        if(flag){
            _burn(caller,amount);
        }
        return true;
    }
    function mint(uint amount) public onlyOperator returns (bool){
        //this function should not be used because it will break the supply of mfb
        _mint(address(this),amount);
        return true;
    }
    function deposit() public {
        require(balanceOf(msg.sender) > 0,"No sufficient MFB to deposit");
        transfer(_pool_adr,balanceOf(msg.sender));
        emit depositToken(msg.sender, balanceOf(msg.sender));
    }
}
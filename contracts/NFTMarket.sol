// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;
import "./contracts/token/ERC20/IERC20.sol";
import "./contracts/token/ERC721/IERC721.sol";
import "./Access.sol";
import "./IMFBToken.sol";
import "./IMFEToken.sol";
import "./AddressCenter.sol";

contract NFTMarket{
    //address
    AddressCenter adrCenter = AddressCenter(0x6dd9D2dbfFe58993102cfF7a034649357f9E0f5c);
    Access acs = Access(0x88C96DCcD894E4bcb152eE26ab37888936f47074);
    address _mfb_adr;
    address _nft_contract;
    address _pool_adr;
    address _op_wallet;
    bool public is_init = false;
    //modfier
    modifier onlyOperator{
        require(acs.operators(msg.sender),"Require operator permission");
        _;
    }
    //variables
    mapping(uint => bool) public selling;
    mapping(uint => uint) public prices;
    uint public burnRate = 4;
    uint public pool_fee = 4;
    uint public op_fee = 2;
    //event
    event toAddress(address buyer, uint amount, string t_type);
    event buyOrSell(bool isSell, address seller, uint id, uint price);
    event close(uint id);

    //initialize
    function initialize_adr() public onlyOperator {
        _mfb_adr = adrCenter.mfb();
        _nft_contract = adrCenter.nft();
        _pool_adr = adrCenter.mfb_pool();
        _op_wallet = adrCenter.op_wallet();
    }
    //fee setting
    function setBurn (uint rate) public onlyOperator returns (bool) {
        burnRate = rate;
        return true;
    }
    function setOpFee (uint rate) public onlyOperator returns (bool) {
        op_fee = rate;
        return true;
    }
    function setPoolRate (uint rate) public onlyOperator returns (bool) {
        pool_fee = rate;
        return true;
    }
    //fee setting end
    //拍賣NFT
    function sellNFT(uint id, uint price) public{
        require(IERC721(_nft_contract).ownerOf(id) == msg.sender,"Your're not the token owner");
        require(selling[id] == false,"NFT is already on sale");
        require(IMFEToken(_nft_contract).getState(id) == 0,"This nft is not available for sale now");
        require(IERC721(_nft_contract).getApproved(id) == address(this),"This token is not yet approved to the contract");
        selling[id] = true;
        prices[id] = price;
        IMFEToken(_nft_contract).stateChange(id,3);
        emit buyOrSell(true, msg.sender, id, price);
    }
    //購買NFT
    function buyNFT(uint id) public{
        require(IERC20(_mfb_adr).balanceOf(msg.sender) >= prices[id],"You don't have enough MFB to buy this NFT");
        require(selling[id] == true,"NFT is not on sale");
        address ori_owner = IERC721(_nft_contract).ownerOf(id);
        //ERC20 transfer
        IMFBToken(_mfb_adr).approve2Contract(msg.sender,prices[id]);
        uint actual = 100 - burnRate - pool_fee - op_fee;
        IERC20(_mfb_adr).transferFrom(msg.sender,ori_owner,actual * prices[id] / 100);
        emit toAddress(msg.sender,actual * prices[id] / 100,"purchase");
        IERC20(_mfb_adr).transferFrom(msg.sender,_pool_adr, pool_fee * prices[id] / 100);
        emit toAddress(msg.sender, pool_fee * prices[id] / 100 ,"pool");
        IERC20(_mfb_adr).transferFrom(msg.sender,_op_wallet,op_fee * prices[id] / 100);
        emit toAddress(msg.sender,op_fee* prices[id] / 100,"operator");  
        IMFBToken(_mfb_adr).burnOnBuy(msg.sender,prices[id] / 100 * burnRate);
        //721
        IMFEToken(_nft_contract).stateChange(id,0);
        IERC721(_nft_contract).transferFrom(ori_owner,msg.sender,id);      
        emit buyOrSell(false, msg.sender, id, prices[id]);
        selling[id] = false;
        prices[id] = 0;
    }
    //中止拍賣
    function endSale(uint id) public{
        require(selling[id] == true,"NFT is not on sale");
        require(IERC721(_nft_contract).ownerOf(id) == msg.sender,"Your're not the token owner");
        emit close(id);
        selling[id] = false;
        prices[id] = 0;
        IMFEToken(_nft_contract).stateChange(id,0);
    }
}
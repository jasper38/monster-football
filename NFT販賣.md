## NFT上架流程
### Market.sol
1. 調用  sellNFT(uint id, uint price) 檢查是否已拍賣、是否為Token owner、Token狀態是否能交易
>	ex:
>>	    id: 1
>>	    price: 12000000000000000000(MFB)
- 可調用price(id)獲取nft價錢，再用selling(id)獲取NFT拍賣狀態
- 請先調用MFEToken approve(id, market_contract) 將NFT權限給予市場合約
2. 調用 Market endSale(id) 手動下架
3. 使用 buyNFT(uint id) 購買NFT
   - 此處檢查address MFB餘額是否足夠，NFT是否拍賣中 
   - 請監聽toAddress(address,uint,"purchase")及toAddress(address,uint,"pool")
   - 轉至獎勵池、營運錢包、販賣者 

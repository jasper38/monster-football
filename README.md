# Monster Football
## Update List
- 1/12 優化部分合約內容，新增MFB買賣合約(測試中)
    - NFT token 鑄造時會自動產生URI
    - 盲盒可查詢販賣中盲盒ID
    - 根據block hash作亂數生成
    - MFB C2C買賣
- 1/14 修正BlindBox流程文件
  - [測試專案](https://gitlab.com/jasper38/metamask-smart-contract-test)
  - Gold合約部屬
- 1/19 新增GoldPool、addressCenter
  - 打工獎勵設置及領取
  - 合約地址設置中心、簡化部屬流程
  - 新增NFT URI prefix設置
- 1/25 修改盲盒購買價格計算方式
  - 參考pancake swap [MFB-BUSD價格](https://testnet.bscscan.com/address/0x17F99D1dB8eAAfE9a58843fdF4B45195f9E6dBB9) 
  - NFT狀態更新function簡化
  - 移除MFBMarket.sol
- 1/26 移除invite修改盲盒購買方法
- 2/15 新增簽章
  - 盲盒購買方式更新 -> 新增簽章
  - 獎勵設置方式更新 -> 新增簽章
  - 移除GoldPool.sol，將原功能移至GoldToken.sol中
## TO-DO
### NFT
- [x] NFT鑄造
- [x] NFT孵化
- [x] NFT打工狀態
- [x] NFT交易
- [X] NFT設定狀態(活力點)
### BEP-20(MFB)
- [x] MFB質押 => Pancake swap
- [x] MFB購買盲盒 => 獎勵池、營運錢包
- [ ] 高級打工獎勵
### BEP-20(GOLD)
- [x] 打工獎勵
- [x] 消耗代幣(burn)
- [ ] 戰鬥獎勵
### LP Pool
- [x] 獎勵設置
- [x] 獎勵領取(MFB)
- [x] 獎勵領取(GOLD)
### Blind box
- [x] 盲盒刊登紀錄
- [x] 盲盒販賣紀錄
- [x] 地址盲盒數量紀錄及更新
- [x] 地址盲盒單日購買數量紀錄及更新
- [x] 購買價格參考pancake swap
### Access
- [x] 權限控管
### Address Center
- [x] 合約互動控管

## Deploy
1. 先部屬 Access.sol
2. 紀錄 Access.sol合約位置並更新其他合約內容
3. 部屬 AddressCenter.sol，紀錄位置更新至其他合約中
4. 部署其他合約，使用AddressCenter.initialize_1更新合約地址
5. 使用其他合約前先調用initialize_adr初始化互動合約地址
## Usage
[盲盒流程](https://gitlab.com/jasper38/monster-football/-/blob/main/%E7%9B%B2%E7%9B%92%E6%B5%81%E7%A8%8B.md)
## Contracts
| 合約 | 地址 | 測試鏈部署費 |
| ----------- | ----------- | ----------- |
|Access.sol|[0x88C96DCcD894E4bcb152eE26ab37888936f47074](https://testnet.bscscan.com/address/0x88C96DCcD894E4bcb152eE26ab37888936f47074)|0.002BNB|
|MFBPool.sol|[0xdED7631f0e8792747DBDbAe87Ef940416c13c362](https://testnet.bscscan.com/address/0xdED7631f0e8792747DBDbAe87Ef940416c13c362)|0.01202967BNB|
|BlindBox.sol|[0xfcE936253D3930d69446ddd4839808a6E4E01c31](https://testnet.bscscan.com/address/0xfcE936253D3930d69446ddd4839808a6E4E01c31)|0.03360386BNB|
|MFBToken.sol|[0x443545A02fA5275a281F8AfB67929F1a76E195A1](https://testnet.bscscan.com/address/0x443545A02fA5275a281F8AfB67929F1a76E195A1)|0.02403152BNB|
|MFEToken.sol|[0xa37FbE4685b6E8cA59787bF959Fd196574079A53](https://testnet.bscscan.com/address/0xa37FbE4685b6E8cA59787bF959Fd196574079A53)|0.04180544BNB|
|NFTMarket.sol|[0x2535D3C2b3DA5FC4F1450B4293962e06d2f5B05E](https://testnet.bscscan.com/address/0x2535D3C2b3DA5FC4F1450B4293962e06d2f5B05E)|0.02065155BNB|
|Gold.sol|[0x95Af0f640798D8bB18875993170991F5F7418B1F](https://testnet.bscscan.com/address/0x95Af0f640798D8bB18875993170991F5F7418B1F)|0.02134774BNB|
|RebatePool.sol|[0x7B72CD16D4D2436a1Ef7Ff1BAa760C475C5B06Ab](https://testnet.bscscan.com/address/0x7B72CD16D4D2436a1Ef7Ff1BAa760C475C5B06Ab)|0.01318782BNB|
|AddressCenter.sol|[0x6dd9D2dbfFe58993102cfF7a034649357f9E0f5c](https://testnet.bscscan.com/address/0x6dd9D2dbfFe58993102cfF7a034649357f9E0f5c)|0.0082844BNB|


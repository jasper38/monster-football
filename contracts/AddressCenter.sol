// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;
import "./contracts/access/Ownable.sol";

contract AddressCenter is Ownable{
    address public blindbox;
    address public gold;
    address public mfb;
    address public mfb_pool;
    address public nft;
    address public nft_market;
    address public op_wallet;
    address public pancake;
    address public pancake_router;
    address public channel;
    address public gamefi;
    address public rebate;
    function initialize_1(address box, address gd, address mfb_t, address mfb_p, address nft_t, address nft_m, address pancake_adr, address router_adr, address channel_adr, address game_adr, address rebate_adr) 
    public 
    onlyOwner
    {
        blindbox = box;
        gold = gd;
        mfb = mfb_t;
        mfb_pool = mfb_p;
        nft = nft_t;
        nft_market = nft_m;
        pancake = pancake_adr;
        pancake_router = router_adr;
        channel = channel_adr;
        gamefi = game_adr;
        rebate = rebate_adr;
    }
    function setWallet(address wallet) public onlyOwner {
        op_wallet = wallet;
    }
}
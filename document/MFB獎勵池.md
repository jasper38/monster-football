# MFBPool.sol
## 領取獎勵
- function claimReward
- 領取獎勵
- cost: 0.00055297BNB

## 設定獎勵
- function setReward
- 設定指定地址的獎勵數
- onlyOperator, trigger by contract
- body:
```
{
    "user": "0x1ea2ddba5801Fc01EF4Ab4436874f27b08e28c0c",
    "reward": 100000000000000000000
}
```

## 查詢獎勵
- function rewards
- body:
```
{
    "0x1ea2ddba5801Fc01EF4Ab4436874f27b08e28c0c"
}
```
- response:
```
{
    "reward": 100000000000000000000
}
```